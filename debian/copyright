Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: iwd
Source: https://mirrors.edge.kernel.org/pub/linux/network/wireless/

Files: *
Copyright: 2013-2018 Intel Corporation. All rights reserved.
           2016 Markus Ongyerth. All rights reserved.
License: LGPL-2.1+

Files: ell/dbus-client.*
Copyright: 2011-2014 Intel Corporation. All rights reserved.
           2017 Codecoup. All rights reserved.
License: LGPL-2.1+

Files: linux/nl80211.h
Copyright: 2006-2010 Johannes Berg <johannes@sipsolutions.net>
           2008 Michael Wu <flamingice@sourmilk.net>
           2008 Luis Carlos Cobo <luisca@cozybit.com>
           2008 Michael Buesch <m@bues.ch>
           2008, 2009 Luis R. Rodriguez <lrodriguez@atheros.com>
           2008 Jouni Malinen <jouni.malinen@atheros.com>
           2008 Colin McCabe <colin@cozybit.com>
           2015-2017  Intel Deutschland GmbH
License: ISC
 Permission to use, copy, modify, and/or distribute this software for any
 purpose with or without fee is hereby granted, provided that the above
 copyright notice and this permission notice appear in all copies.
 .
 THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

Files: debian/*
Copyright: 2018 Andreas Henriksson <andreas@fatal.se>
License: LGPL-2.1+

License: LGPL-2.1+
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 .
 On Debian systems, the complete text of the GNU Lesser General Public
 License version 2.1 can be found in "/usr/share/common-licenses/LGPL-2.1".
